package net.serenitybdd.screenplay.winium.actions;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Keys;

import com.google.common.collect.Lists;

import net.serenitybdd.screenplay.Interaction;

public abstract class EnterDeskValue implements Interaction {
	
	protected final String theText;
    protected final List followedByKeys;
	
    public EnterDeskValue(String theText) {
		this.theText = theText;
		this.followedByKeys = new ArrayList<>();
	}
    
    public EnterDeskValue thenHit(Keys... keys) {
        this.followedByKeys.addAll(Lists.newArrayList(keys));
        return this;
    }
    
}
