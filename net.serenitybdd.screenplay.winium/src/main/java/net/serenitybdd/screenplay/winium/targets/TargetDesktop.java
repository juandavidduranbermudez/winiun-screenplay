package net.serenitybdd.screenplay.winium.targets;

import org.openqa.selenium.WebElement;

public abstract class TargetDesktop {
	
	protected final String targetElementName;

	public TargetDesktop(String targetElementName) {
		this.targetElementName = targetElementName;
	}

	@Override
	public String toString() {
		return targetElementName;
	}
	
	public static TargetBuilderDesktop the(String targetElementName) {
		return  new TargetBuilderDesktop(targetElementName);
	}
	
    public abstract WebElement resolve();


}
