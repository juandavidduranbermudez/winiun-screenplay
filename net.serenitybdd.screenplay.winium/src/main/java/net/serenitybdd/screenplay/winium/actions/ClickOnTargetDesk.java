package net.serenitybdd.screenplay.winium.actions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.winium.targets.TargetDesktop;

public class ClickOnTargetDesk implements Interaction {
	
	private TargetDesktop targetDesktop;
		
	public ClickOnTargetDesk(TargetDesktop targetDesktop) {
		this.targetDesktop = targetDesktop;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		targetDesktop.resolve().click();
	}

}
