package net.serenitybdd.screenplay.winium.targets;

import org.openqa.selenium.By;

public class TargetBuilderDesktop {

	private String targetElementName;

	public TargetBuilderDesktop(String targetElementName) {
		this.targetElementName = targetElementName;
	}
	
	public ByTargetDesktop located(By locator) {
        return new ByTargetDesktop(targetElementName, locator);
    }
	
	
}
