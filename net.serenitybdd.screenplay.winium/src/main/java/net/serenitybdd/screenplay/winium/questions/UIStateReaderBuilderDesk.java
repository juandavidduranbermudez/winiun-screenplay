package net.serenitybdd.screenplay.winium.questions;

import net.serenitybdd.screenplay.winium.questions.constant.DeskAttribute;
import net.serenitybdd.screenplay.winium.targets.TargetDesktop;

public class UIStateReaderBuilderDesk {
	
	private TargetDesktop targetDesktop;
	
	public UIStateReaderBuilderDesk(TargetDesktop targetDesktop) {
		this.targetDesktop=targetDesktop;
	}
	
	public String forAttribute(DeskAttribute attribute) {
		return targetDesktop.resolve().getAttribute(attribute.getAttrbute());
	}
	

}
