package net.serenitybdd.screenplay.winium.actions;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import net.serenitybdd.screenplay.winium.targets.TargetDesktop;

public class EnterDesk {
	
	private final String theText;

	public EnterDesk(String theText) {
		this.theText = theText;
	}
	
	public static EnterDesk theValue(String text) {
        return new EnterDesk(text);
    }
	
	public EnterDeskValue into(TargetDesktop targetDesktop) {
        return instrumented(EnterValueIntoTargetDesk.class, theText, targetDesktop);
    }

}
