package net.serenitybdd.screenplay.winium.actions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.winium.DesktopApp;

public class OpenPageDesk implements Interaction {
	
	private String pathApp;
	
	public OpenPageDesk(String pathApp) {
		super();
		this.pathApp = pathApp;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		DesktopApp.openApp(pathApp);
	}

}
