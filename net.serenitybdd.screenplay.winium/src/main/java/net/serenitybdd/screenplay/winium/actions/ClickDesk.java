package net.serenitybdd.screenplay.winium.actions;

import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.winium.targets.TargetDesktop;

import static net.serenitybdd.screenplay.Tasks.instrumented;
public class ClickDesk {
	
	public static Interaction on(TargetDesktop targetDesktop) {
		return instrumented(ClickOnTargetDesk.class, targetDesktop);
	}

}
