package net.serenitybdd.screenplay.winium.actions;

import org.openqa.selenium.Keys;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.winium.targets.TargetDesktop;

public class HitTargetDesk implements Interaction {
	
	private Keys[] keys;
    private TargetDesktop targetDesktop;
    
	public HitTargetDesk(Keys[] keys, TargetDesktop targetDesktop) {
		this.keys = keys;
		this.targetDesktop = targetDesktop;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		targetDesktop.resolve().sendKeys(keys);		
	}
	
	

}
