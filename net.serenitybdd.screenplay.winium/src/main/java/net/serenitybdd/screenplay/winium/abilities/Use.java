package net.serenitybdd.screenplay.winium.abilities;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.serenitybdd.screenplay.Ability;

public class Use implements Ability{
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public Use() {
		try {
			Process process = new ProcessBuilder("src/test/resources/driver/Winium.Desktop.Driver.exe").start();
			logger.info("Driver iniciado...");
		} catch (IOException e) {
			logger.warn("Fallo iniciacion de Driver", e);
		}
	}
	
	public static Use DesktopApplications() {
		return new Use();
	}

}
