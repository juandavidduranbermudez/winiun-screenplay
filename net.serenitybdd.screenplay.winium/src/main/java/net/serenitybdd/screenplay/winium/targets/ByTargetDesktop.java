package net.serenitybdd.screenplay.winium.targets;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import net.serenitybdd.screenplay.winium.DesktopApp;

public class ByTargetDesktop extends TargetDesktop {
	
	private String targetElementName;
	private By locator;
	
	public ByTargetDesktop(String targetElementName, By locator) {
		super(targetElementName);
		this.locator = locator;
	}

	@Override
	public WebElement resolve() {
		return DesktopApp.TargetResolver(locator);
	}
	
	

}
