package net.serenitybdd.screenplay.winium.actions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.winium.targets.TargetDesktop;

public class EnterValueIntoTargetDesk extends EnterDeskValue{
	
	private TargetDesktop targetDesktop;

	public EnterValueIntoTargetDesk(String theText, TargetDesktop targetDesktop) {
		super(theText);
		this.targetDesktop = targetDesktop;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		targetDesktop.resolve().sendKeys(theText);
	}
	
}
