package net.serenitybdd.screenplay.winium;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DesktopApp {
	
	public static DesktopOptions DESKTOP_OPTIONS;
	public static WiniumDriver DRIVER;
	private static final String REMOTE_ADDRESS="http://localhost:9999";
	private static Logger logger = LoggerFactory.getLogger(DesktopApp.class.getName()); 
	
	public static void openApp(String pathApp) {
		DESKTOP_OPTIONS=new DesktopOptions();
		DESKTOP_OPTIONS.setApplicationPath(pathApp);
		setDriver(DESKTOP_OPTIONS);
	}
	
	public static WebElement TargetResolver(By locator) {
		DESKTOP_OPTIONS = new DesktopOptions();
		DESKTOP_OPTIONS.setDebugConnectToRunningApp(true);
		setDriver(DESKTOP_OPTIONS);
		return DRIVER.findElement(locator);
	}
	
	public static DesktopOptions getDesktopOptions(boolean b) {
		DESKTOP_OPTIONS = new DesktopOptions();
		DESKTOP_OPTIONS.setDebugConnectToRunningApp(b);
		return DESKTOP_OPTIONS;
	}
	
	public static WiniumDriver getWiniumDriver(boolean b) {
		DESKTOP_OPTIONS = new DesktopOptions();
		DESKTOP_OPTIONS.setDebugConnectToRunningApp(b);
		setDriver(DESKTOP_OPTIONS);
		return DRIVER;
	}	
	
	
	private static void setDriver(DesktopOptions desktopOptions) {
		try {
			DRIVER = new WiniumDriver(new URL(REMOTE_ADDRESS),desktopOptions);
		} 
		catch (MalformedURLException e) {
			logger.warn("Fallo Driver", e);
		}
	}

}
