package net.serenitybdd.screenplay.winium.questions;

import net.serenitybdd.screenplay.winium.targets.TargetDesktop;

public class TextDesk{
	
	public static UIStateReaderBuilderDesk of(TargetDesktop targetDesktop) {
		return new UIStateReaderBuilderDesk(targetDesktop);
	}
}
