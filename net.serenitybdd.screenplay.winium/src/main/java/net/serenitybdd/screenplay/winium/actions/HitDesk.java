package net.serenitybdd.screenplay.winium.actions;

import java.util.Arrays;

import org.openqa.selenium.Keys;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.winium.targets.TargetDesktop;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class HitDesk {
	
	private Keys[] keys;

	public HitDesk(Keys[] keys) {
		this.keys = keys;
	}
	
	public static HitDesk the(Keys...keys ) {
		return new HitDesk(Arrays.copyOf(keys,keys.length));
	}
	
	public Performable into(TargetDesktop targetDesktop) {
		return instrumented(HitTargetDesk.class, keys,targetDesktop);
	}

}
