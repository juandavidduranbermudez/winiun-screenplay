package net.serenitybdd.screenplay.winium.actions;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class OpenDesk {
	
	public static OpenPageDesk browserOnDesk(String pathApp) {
		return instrumented(OpenPageDesk.class,pathApp);
	}

}
