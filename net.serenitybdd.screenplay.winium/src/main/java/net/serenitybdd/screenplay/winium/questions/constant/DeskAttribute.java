package net.serenitybdd.screenplay.winium.questions.constant;

public enum DeskAttribute {
	
	CLASSNAME("ClassName"),
	CONTROLTYPE("ControlType"),
	CULTURE("Culture"),
	AUTOMATIONID("AutomationId"),
	LOCALIZEDCONTROLTYPE("LocalizedControlType"),
	NAME("Name"),
	PROCESSID("ProcessId"),
	RUNTIMEID("RuntimeId"),
	ISPASSWORD("IsPassword"),
	ISCONTROLELEMENT("IsControlElement"),
	ISCONTENTELEMENT("IsContentElement");

	private final String attrbute; 
	
	DeskAttribute(String attrbute) {
		this.attrbute = attrbute;
	}

	public String getAttrbute() {
		return attrbute;
	} 

	

}