package co.com.bancolombia.certification.winiun_test.WiniunTest.user_interface;

import org.openqa.selenium.By;

import net.serenitybdd.screenplay.winium.targets.TargetDesktop;

public class CalculadoraDeskApp {
	
	public static final TargetDesktop BTN_TRES = TargetDesktop.the("BOTON NUMERO 3 DE LA CALCULADORA").located(By.id("num3Button"));
	public static final TargetDesktop BTN_SEIS = TargetDesktop.the("BOTON NUMERO 6 DE LA CALCULADORA").located(By.id("num6Button"));
	public static final TargetDesktop BTN_SUMAR = TargetDesktop.the("BOTON SUMAR DE LA CALCULADORA").located(By.id("plusButton"));
	public static final TargetDesktop BTN_IGUAL = TargetDesktop.the("BOTON IGUAL DE LA CALCULADORA").located(By.id("equalButton"));
	public static final TargetDesktop LBL_RESULTADO = TargetDesktop.the("BOTON IGUAL DE LA CALCULADORA").located(By.id("CalculatorResults"));
	
}
