package co.com.bancolombia.certification.winiun_test.WiniunTest.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.winium.actions.OpenDesk;

public class AbroLaCalculadora implements Task{
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(OpenDesk.browserOnDesk("C:\\Windows\\System32\\calc.exe"));
	}

	public static AbroLaCalculadora deWindow() {
		return new AbroLaCalculadora();
	}

}
