package co.com.bancolombia.certification.winiun_test.WiniunTest.task;

import org.openqa.selenium.WebElement;

import co.com.bancolombia.certification.winiun_test.WiniunTest.user_interface.CalculadoraDeskApp;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.winium.actions.ClickDesk;
import net.serenitybdd.screenplay.winium.actions.EnterDesk;
import net.serenitybdd.screenplay.winium.questions.TextDesk;
import net.serenitybdd.screenplay.winium.questions.constant.DeskAttribute;

public class Sumar implements Task {

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		//actor.attemptsTo(ClickDesk.on(CalculadoraDeskApp.BTN_TRES));
		actor.attemptsTo(EnterDesk.theValue("1").into(CalculadoraDeskApp.BTN_TRES));
		actor.attemptsTo(ClickDesk.on(CalculadoraDeskApp.BTN_SUMAR));
		actor.attemptsTo(EnterDesk.theValue("4").into(CalculadoraDeskApp.BTN_TRES));
		//actor.attemptsTo(ClickDesk.on(CalculadoraDeskApp.BTN_SEIS));
		actor.attemptsTo(ClickDesk.on(CalculadoraDeskApp.BTN_IGUAL));
		
		
		System.out.println("-->"+TextDesk.of(CalculadoraDeskApp.LBL_RESULTADO).forAttribute(DeskAttribute.NAME));
		
	}

	public static Performable Numeros() {
		return new Sumar();
	}

}
