package co.com.bancolombia.certification.winiun_test.WiniunTest.feature;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/feature/demo.feature",glue="co.com.bancolombia.certification.winiun_test.WiniunTest.step_definitions",snippets=SnippetType.CAMELCASE)

public class RunnerTags {

}
