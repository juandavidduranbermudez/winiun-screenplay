package co.com.bancolombia.certification.winiun_test.WiniunTest.step_definitions;

import co.com.bancolombia.certification.winiun_test.WiniunTest.task.AbroLaCalculadora;
import co.com.bancolombia.certification.winiun_test.WiniunTest.task.Sumar;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.winium.abilities.Use;

public class Definitions {
	
	private Actor actor = Actor.named("actor");
	
	@Before
	public void configuracionInicial() {
		//actor.can(BrowseTheWeb.with(hisBrowser));
		actor.can(Use.DesktopApplications());
	}
	
	@When("^abro la calculadora de window$")
	public void abroLaCalculadoraDeWindow() throws Exception {
	    // Write code here that turns the phrase above into concrete actions
		actor.wasAbleTo(AbroLaCalculadora.deWindow());
	}
	
	@Then("^Realizo una suma$")
	public void realizoUnaSuma() throws Exception {
	    // Write code here that turns the phrase above into concrete actions
		actor.attemptsTo(Sumar.Numeros());
	}
	
	

}
